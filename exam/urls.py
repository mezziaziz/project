from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.index , name='index'),
    url(r'^(?P<exam_id>[0-9]+)/$', views.detail,name='detail'),
    url(r'^(?P<exam_id>[0-9]+)/reponse/$', views.reponse, name='reponse'),
    url(r'^(?P<exam_id>[0-9]+)/edit/$', views.edit, name='edit'),
    url(r'^(?P<Quest_id>[0-9]+)/editfree/$', views.editfree, name='editfree'),
    url(r'^(?P<Quest_id>[0-9]+)/editmulti/$', views.editmulti, name='editmulti'),
    url(r'^(?P<Quest_id>[0-9]+)/editone/$', views.editone, name='editone'),
    url(r'^create/$', views.create_view, name='create_view'),
    url(r'^createQuest/$', views.createExam, name='createExam'),
    url(r'^secondeQuest/$', views.secondeQuest, name='secondeQuest'),
    url(r'^(?P<exam_id>[0-9]+)/addQuest/$', views.addQuest, name='addQuest'),
    url(r'^(?P<exam_id>[0-9]+)/deleteEXam/$', views.deleteEXam, name='deleteEXam'),
    url(r'^(?P<question_id>[0-9]+)/deleteQuest/$', views.deleteQuest, name='deleteQuest'),
    url(r'^(?P<exam_id>[0-9]+)/passe/$', views.passe, name='passe'),
    url(r'^(?P<exam_id>[0-9]+)/passe/(?P<Quest_id>[0-9]+)$', views.passeQuest, name='passeQuest'),
    url(r'^(?P<exam_id>[0-9]+)/result/$', views.result, name='result'),
    url(r'^(?P<exam_id>[0-9]+)/invitating/$', views.invitating, name='invitating'),
    url(r'^addUser/$', views.addUser, name='addUser'),
    url(r'^consultUsers/$', views.consultUsers, name='consultUsers'),
    url(r'^sendMail/$', views.sendMail, name='sendMail'),
    url(r'^(?P<exam_id>[0-9]+)/passeExam/$', views.passeStudent, name='passeStudent'),
    url(r'^(?P<exam_id>[0-9]+)/passeExam/(?P<Quest_id>[0-9]+)$', views.passeQuestStudent, name='passeQuestStudent'),
    url(r'^(?P<exam_id>[0-9]+)/resultExam/$', views.resultStudent, name='resultStudent'),
    url(r'^becameProf/$', views.becameProf, name='becameProf'),
    url(r'^MyExams/$', views.MyExams , name='MyExams'),
    url(r'^correction/$', views.correction , name='correction'),
    url(r'^correction/(?P<exam_id>[0-9]+)/$', views.correctionExam , name='correctionExam'),
    url(r'^certified/$', views.certified , name='certified'),
    url(r'^certified/(?P<exam_id>[0-9]+)/$', views.imprimeCertif , name='imprimeCertif'),
    url(r'^profile/$', views.profile, name='profile'),
    url(r'^updateProfile/$', views.updateProfile, name='updateProfile'),

              ]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)