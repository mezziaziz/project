from django.db import models
from django.contrib.auth.models import User
from authentified.models import Profile
import datetime
# Create your models here.

class Exam(models.Model):
    name = models.CharField(max_length=50)
    logo = models.ImageField(upload_to='exam_image',blank=True,null=True)
    description = models.CharField(max_length=50,default='')
    category = models.CharField(max_length=50,default='')
    score = models.IntegerField(default=0)
    timer = models.IntegerField(default=0)
    type=models.CharField(max_length=50,default='')
    certified=models.BooleanField(default=False)
    user=models.ForeignKey(User,on_delete=models.CASCADE,default=1)



    def __str__(self):
         return self.name

class Question(models.Model):
    examen = models.ForeignKey(Exam,on_delete=models.CASCADE)
    description = models.CharField(max_length=1000,default='')
    points = models.IntegerField(default=0)
    header_text = models.CharField(max_length=1000,default='')
    footer_text = models.CharField(max_length=1000,default='')
    success_msg = models.CharField(max_length=1000,default='')
    fail_msg = models.CharField(max_length=1000,default='')


    def __str__(self):
         return self.description

class one_answer(models.Model):
    question = models.ForeignKey(Question,on_delete=models.CASCADE)
    answer_variant = models.CharField(max_length=50)
    answer_description = models.CharField(max_length=1000)
    points = models.IntegerField(default=0)

    def __str__(self):
        return self.answer_variant
class Multi_choice_answer(models.Model):
    question = models.ForeignKey(Question,on_delete=models.CASCADE)
    multi_variant = models.CharField(max_length=50)
    multi_description = models.CharField(max_length=1000)
    points = models.IntegerField(default=0)

    def __str__(self):
        return self.multi_variant

class Free_text(models.Model):
    question = models.ForeignKey(Question,on_delete=models.CASCADE)
    text = models.CharField(max_length=1000)


    def __str__(self):
        return self.text

class Reponse(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    exam=models.ForeignKey(Exam,on_delete=models.CASCADE, related_name='Exam',default=1 )
    One_answer=models.ForeignKey(one_answer,on_delete=models.CASCADE, related_name='One_answer')
    multi_choice_answer=models.ForeignKey(Multi_choice_answer,on_delete=models.CASCADE, related_name='multi_choice_answer')
    free_text=models.CharField(max_length=1000,default='')#enregistrer le score ou l id!!!!!!!!!
    score_question=models.IntegerField(default=0)
    nbr_attempt=models.IntegerField(default=1)

class studentClass(models.Model):
    professor=models.ForeignKey(User,on_delete=models.CASCADE,related_name='prof',default=1)
    student=models.ForeignKey(User,on_delete=models.CASCADE,related_name='student',blank=True,null=True)
    exam=models.ForeignKey(Exam,on_delete=models.CASCADE,related_name='exam',blank=True,null=True)
    email_invited=models.CharField(max_length=1000,default="")

class invitation(models.Model):
    prof = models.ForeignKey(User, on_delete=models.CASCADE,related_name='professor',  default=1)
    student = models.ForeignKey(User, on_delete=models.CASCADE,  default=0)
    exams = models.ForeignKey(Exam, on_delete=models.CASCADE,    default=0)
    non_passed=models.BooleanField(default=True)
    attempt=models.IntegerField(default=0)

class certificat(models.Model):
    exam=models.ForeignKey(Exam,on_delete=models.CASCADE,blank=True,null=True)
    student=models.ForeignKey(User,on_delete=models.CASCADE,blank=True,null=True)
    score=models.IntegerField(default=0)
    date = models.DateField(default=datetime.date.today)
#date a verifier