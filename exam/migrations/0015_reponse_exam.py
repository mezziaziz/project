# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-07-31 14:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0014_auto_20180731_1352'),
    ]

    operations = [
        migrations.AddField(
            model_name='reponse',
            name='exam',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='Exam', to='exam.Exam'),
        ),
    ]
