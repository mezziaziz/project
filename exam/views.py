from django.shortcuts import render,redirect
from .models import Question,Exam,one_answer,Free_text,Multi_choice_answer,Reponse,studentClass,invitation,certificat
# Create your views here.
from django.contrib.auth.models import User
from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse
from django.template import loader
from random import *
from authentified.models import Profile
from django.core.mail import send_mail
from django.core.exceptions import ObjectDoesNotExist
import pusher
def index(request):



    if request.user.is_authenticated():


        profile=Profile.objects.get(user=request.user)
        if profile.is_prof==True:
            all_exam = Exam.objects.filter(user=request.user)
            if id in request.session:
                del request.session['id']
            my_template='exam/baseProf.html'
            context = {
                'my_template': my_template,
                'all_exam': all_exam,
            }
            return render(request, 'exam/index.html',context)
        else:
            my_template='exam/base.html'
            invite =invitation.objects.filter(student=request.user)
            context={
                'my_template': my_template,
                'invite':invite,
            }
            return render(request, 'exam/studentIndex.html',context)




    else:
        return render(request, "firstPage/index.html")



def edit(request,exam_id):
    exam = Exam.objects.get(pk=exam_id)
    if request.method=="POST":
        examName = request.POST['examName']
        ExamDesc = request.POST['ExamDesc']
        category = request.POST['category']

        exam.name = examName
        exam.description = ExamDesc
        exam.category = category
        exam.save()
        return redirect('/exam/')
    template = loader.get_template('exam/edit.html')
    my_template = 'exam/baseProf.html'

    context = {
        'examen_id': exam_id,
      'examen': exam,
        'my_template': my_template,

    }

    return HttpResponse(template.render(context, request))



def detail(request,exam_id):


    exam = Exam.objects.get(pk=exam_id)
    template = loader.get_template('exam/les_questions.html')
    my_template = 'exam/baseProf.html'

    context = {
        'my_template': my_template,

        'examen_id': exam_id,
      'examen': exam,

    }

    return HttpResponse(template.render(context, request))

def editone(request,Quest_id):
    question = Question.objects.get(pk=Quest_id)
    if request.user.is_authenticated():
        if request.method == 'POST':
            Description = request.POST['Description']
            Header_text = request.POST['Header_text']
            Footer_text = request.POST['Footer_text']
            Success_msg = request.POST['Success_msg']
            Fail_msg = request.POST['Fail_msg']
            points = request.POST['points']
            question.description=Description
            question.header_text = Header_text
            question.footer_text = Footer_text
            question.success_msg = Success_msg
            question.fail_msg = Fail_msg
            question.points = points
            question.save()
            ID1=request.POST['id1']
            ID2 = request.POST['id2']
            ID3 = request.POST['id3']
            ans1=one_answer.objects.get(pk=ID1)
            ans2=one_answer.objects.get(pk=ID2)
            ans3=one_answer.objects.get(pk=ID3)

            answer_variant = request.POST['answer_variant1']
            answer_description = request.POST['answer_description1']
            pointsAns = request.POST['points1']
            answer_variant2 = request.POST['answer_variant2']
            answer_description2 = request.POST['answer_description2']
            pointsAns2 = request.POST['points2']
            answer_variant3 = request.POST['answer_variant3']
            answer_descriptio3 = request.POST['answer_description3']
            pointsAn3 = request.POST['points3']

            ans1.answer_variant = answer_variant
            ans1.answer_description = answer_description
            ans1.points = pointsAns
            ans1.save()
            ans2.answer_variant = answer_variant2
            ans2.answer_description = answer_description2
            ans2.points = pointsAns2
            ans2.save()
            ans3.answer_variant = answer_variant3
            ans3.answer_description = answer_descriptio3
            ans3.points = pointsAn3
            ans3.save()



            return redirect("/exam/")
        else:
            template = loader.get_template('exam/editOneAnser.html')
            context = {
                'question': question,

            }

            return HttpResponse(template.render(context, request))

def editmulti(request,Quest_id):
    question = Question.objects.get(pk=Quest_id)
    if request.user.is_authenticated():
        if request.method == 'POST':
            Description = request.POST['Description']
            Header_text = request.POST['Header_text']
            Footer_text = request.POST['Footer_text']
            Success_msg = request.POST['Success_msg']
            Fail_msg = request.POST['Fail_msg']
            points = request.POST['points']
            question.description=Description
            question.header_text = Header_text
            question.footer_text = Footer_text
            question.success_msg = Success_msg
            question.fail_msg = Fail_msg
            question.points = points
            question.save()
            ID1=request.POST['id1']
            ID2 = request.POST['id2']
            ID3 = request.POST['id3']
            ans1=Multi_choice_answer.objects.get(pk=ID1)
            ans2=Multi_choice_answer.objects.get(pk=ID2)
            ans3=Multi_choice_answer.objects.get(pk=ID3)

            answer_variant = request.POST['answer_variant1']
            answer_description = request.POST['answer_description1']
            pointsAns = request.POST['points1']
            answer_variant2 = request.POST['answer_variant2']
            answer_description2 = request.POST['answer_description2']
            pointsAns2 = request.POST['points2']
            answer_variant3 = request.POST['answer_variant3']
            answer_descriptio3 = request.POST['answer_description3']
            pointsAn3 = request.POST['points3']

            ans1.multi_variant = answer_variant
            ans1.multi_description = answer_description
            ans1.points = pointsAns
            ans1.save()
            ans2.multi_variant = answer_variant2
            ans2.multi_description = answer_description2
            ans2.points = pointsAns2
            ans2.save()
            ans3.multi_variant = answer_variant3
            ans3.multi_description = answer_descriptio3
            ans3.points = pointsAn3
            ans3.save()



            return redirect("/exam/")
        else:
            template = loader.get_template('exam/EditMulti.html')
            context = {
                'question': question,

            }

            return HttpResponse(template.render(context, request))

def editfree(request,Quest_id):
    question = Question.objects.get(pk=Quest_id)
    if request.user.is_authenticated():
        if request.method == 'POST':
            Description = request.POST['Description']
            Header_text = request.POST['Header_text']
            Footer_text = request.POST['Footer_text']
            Success_msg = request.POST['Success_msg']
            Fail_msg = request.POST['Fail_msg']
            points = request.POST['points']
            question.description=Description
            question.header_text = Header_text
            question.footer_text = Footer_text
            question.success_msg = Success_msg
            question.fail_msg = Fail_msg
            question.points = points
            question.save()
            id = request.POST['id']
            freeText=Free_text.objects.get(pk=id)
            text =request.POST['text']
            freeText.text=text
            freeText.save()
            return redirect("/exam/")
        else:
            template = loader.get_template('exam/EditFreeText.html')
            context = {
                'question': question,

            }

            return HttpResponse(template.render(context, request))

def reponse(request,exam_id):
    exam = Exam.objects.get(pk=exam_id)
    template = loader.get_template('exam/valider.html')


    try:
        choice = one_answer.objects.get(pk=request.POST['choice'])


    except(KeyError):

        exam.total = 0
        exam.save()
        context = {
            'exam': exam,
            'exam_id': exam_id,
            'message':'choix non valide',

        }
        return HttpResponse(template.render(context, request))
    else:
        exam.total = choice.note
        exam.save()
        context = {
            'exam': exam,
            'exam_id': exam_id,

        }
        return HttpResponse(template.render(context, request))

def create_view(request):

     if request.user.is_authenticated():
         return render(request, 'exam/createExam.html')

def createExam(request):
     if request.user.is_authenticated():
        if request.method == 'POST' and request.POST['examName']:
            exam = Exam()
            examName = request.POST['examName']
            ExamDesc = request.POST['ExamDesc']
            category = request.POST['category']
            type = request.POST['type']
            timer = request.POST['timer']
            score = request.POST['score']

            if "certified" in request.POST:
                certified=request.POST['certified']
                if certified =='yes':
                    exam.certified=True
                else:
                    exam.certified=False
            if 'logo' in request.FILES:
                logo = request.FILES['logo']
                exam.logo = logo
            print(timer)
            exam.name = examName
            exam.description = ExamDesc
            exam.category = category
            exam.type=type
            if int(timer) == 0:
                timer = 10000
            exam.timer=timer
            exam.user=request.user
            exam.score=score
            exam.save()
            request.session['id'] = exam.id
            print(request.session['id'])

            context = {
                'exam_id': exam.id,
            }
            return render(request, 'exam/createQuest.html', context)

def secondeQuest(request):
    if request.user.is_authenticated():
        if request.method == 'POST':
            Description = request.POST['Description']
            Header_text = request.POST['Header_text']
            Footer_text = request.POST['Footer_text']
            Success_msg = request.POST['Success_msg']
            Fail_msg = request.POST['Fail_msg']
            points = request.POST['points']
            type=request.POST['type']


            question = Question()
            exam = Exam.objects.get(pk=request.session['id'])
            question.examen = exam
            question.description=Description
            question.header_text = Header_text
            question.footer_text = Footer_text
            question.success_msg = Success_msg
            question.fail_msg = Fail_msg
            question.points = points

            question.save()

            if type =='one':
                answer_variant4=" "
                answer_variant5=" "
                answer_variant6=" "
                answer_variant=request.POST['answer_variant1']
                answer_description=request.POST['answer_description1']
                answer_variant2 = request.POST['answer_variant2']
                answer_description2 = request.POST['answer_description2']
                answer_variant3= request.POST['answer_variant3']
                answer_descriptio3 = request.POST['answer_description3']
                pointsAns=request.POST['pointsAns']
                if 'answer_variant4' in request.POST:
                    answer_variant4 = request.POST['answer_variant4']
                    answer_description4 = request.POST['answer_description4']
                    if answer_variant4 != " ":
                        ans4 = one_answer()
                        ans4.question = question
                        ans4.answer_variant = answer_variant4
                        ans4.answer_description = answer_description4
                        if 'answer_variant5' in request.POST:
                            answer_variant5 = request.POST['answer_variant5']
                            answer_description5 = request.POST['answer_description5']
                            if answer_variant5 != " ":
                                ans5 = one_answer()
                                ans5.question = question
                                ans5.answer_variant = answer_variant5
                                ans5.answer_description = answer_description5
                                if 'answer_variant6' in request.POST:
                                    answer_variant6 = request.POST['answer_variant6']
                                    answer_description6 = request.POST['answer_description6']
                                    if answer_variant6 != " ":
                                        ans6 = one_answer()
                                        ans6.question = question
                                        ans6.answer_variant = answer_variant6
                                        ans6.answer_description = answer_description6




                ans =one_answer()
                ans.question=question
                ans.answer_variant=answer_variant
                ans.answer_description=answer_description


                ans2 =one_answer()
                ans2.question = question
                ans2.answer_variant = answer_variant2
                ans2.answer_description = answer_description2


                ans3=one_answer()
                ans3.question = question
                ans3.answer_variant = answer_variant3
                ans3.answer_description = answer_descriptio3


                if pointsAns=='1':
                    ans.points=points
                elif pointsAns=='2':
                    ans2.points=points
                elif pointsAns=='3':
                    ans3.points=points
                elif pointsAns=='4':
                    ans4.points=points
                elif pointsAns=='5':
                    ans5.points=points
                elif pointsAns=='6':
                    ans6.points=points
                else:
                    ans6.points = points
                ans.save()
                ans2.save()
                ans3.save()
                if answer_variant4 != " ":
                    ans4.save()
                    if answer_variant5 != " ":
                        ans5.save()
                        if answer_variant6 != " ":
                            ans6.save()
            elif type=='free':
                text=request.POST['text']
                free=Free_text()
                free.question=question
                free.text=text
                free.save()
            else:
                answer_variant = request.POST['answer_variant1m']
                answer_description = request.POST['answer_description1m']

                answer_variant2 = request.POST['answer_variant2m']
                answer_description2= request.POST['answer_description2m']

                answer_variant3 = request.POST['answer_variant3m']
                answer_description3 = request.POST['answer_description3m']
                check = request.POST.getlist("check")
                longeur=len(check)
                v1=0
                v2=0
                v3=0
                for i in check:
                    if i=='1':
                        v1=int(points)/longeur
                    elif i=='2':
                        v2=int(points)/longeur
                    else:
                        v3=int(points)/longeur
                multi=Multi_choice_answer()
                multi.question=question
                multi.multi_variant=answer_variant
                multi.multi_description=answer_description
                multi.points = v1

                multi.save()
                multi2= Multi_choice_answer()
                multi2.question = question
                multi2.multi_variant = answer_variant2
                multi2.multi_description = answer_description2
                multi2.points = v2

                multi2.save()
                multi3 = Multi_choice_answer()
                multi3.question = question
                multi3.multi_variant = answer_variant3
                multi3.multi_description = answer_description3
                multi3.points = v3
                multi3.save()

            return render(request, 'exam/createQuest.html')
        else:
            return render(request, 'exam/createQuest.html')

def addQuest(request,exam_id):
    if request.user.is_authenticated():
        if request.method == 'POST':
            Description = request.POST['Description']
            Header_text = request.POST['Header_text']
            Footer_text = request.POST['Footer_text']
            Success_msg = request.POST['Success_msg']
            Fail_msg = request.POST['Fail_msg']
            points = request.POST['points']
            type=request.POST['type']


            question = Question()
            exam = Exam.objects.get(pk=exam_id)
            question.examen = exam
            question.description=Description
            question.header_text = Header_text
            question.footer_text = Footer_text
            question.success_msg = Success_msg
            question.fail_msg = Fail_msg
            question.points = points

            question.save()

            if type =='one':
                answer_variant4=" "
                answer_variant5=" "
                answer_variant6=" "
                answer_variant=request.POST['answer_variant1']
                answer_description=request.POST['answer_description1']
                answer_variant2 = request.POST['answer_variant2']
                answer_description2 = request.POST['answer_description2']
                answer_variant3= request.POST['answer_variant3']
                answer_descriptio3 = request.POST['answer_description3']
                pointsAns=request.POST['pointsAns']
                if 'answer_variant4' in request.POST:
                    answer_variant4 = request.POST['answer_variant4']
                    answer_description4 = request.POST['answer_description4']
                    if answer_variant4 != " ":
                        ans4 = one_answer()
                        ans4.question = question
                        ans4.answer_variant = answer_variant4
                        ans4.answer_description = answer_description4
                        if 'answer_variant5' in request.POST:
                            answer_variant5 = request.POST['answer_variant5']
                            answer_description5 = request.POST['answer_description5']
                            if answer_variant5 != " ":
                                ans5 = one_answer()
                                ans5.question = question
                                ans5.answer_variant = answer_variant5
                                ans5.answer_description = answer_description5
                                if 'answer_variant6' in request.POST:
                                    answer_variant6 = request.POST['answer_variant6']
                                    answer_description6 = request.POST['answer_description6']
                                    if answer_variant6 != " ":
                                        ans6 = one_answer()
                                        ans6.question = question
                                        ans6.answer_variant = answer_variant6
                                        ans6.answer_description = answer_description6




                ans =one_answer()
                ans.question=question
                ans.answer_variant=answer_variant
                ans.answer_description=answer_description


                ans2 =one_answer()
                ans2.question = question
                ans2.answer_variant = answer_variant2
                ans2.answer_description = answer_description2


                ans3=one_answer()
                ans3.question = question
                ans3.answer_variant = answer_variant3
                ans3.answer_description = answer_descriptio3


                if pointsAns=='1':
                    ans.points=points
                elif pointsAns=='2':
                    ans2.points=points
                elif pointsAns=='3':
                    ans3.points=points
                elif pointsAns=='4':
                    ans4.points=points
                elif pointsAns=='5':
                    ans5.points=points
                elif pointsAns=='6':
                    ans6.points=points
                else:
                    ans6.points = points
                ans.save()
                ans2.save()
                ans3.save()
                if answer_variant4 != " ":
                    ans4.save()
                    if answer_variant5 != " ":
                        ans5.save()
                        if answer_variant6 != " ":
                            ans6.save()
            elif type=='free':
                text=request.POST['text']
                free=Free_text()
                free.question=question
                free.text=text
                free.save()
            else:
                answer_variant = request.POST['answer_variant1m']
                answer_description = request.POST['answer_description1m']

                answer_variant2 = request.POST['answer_variant2m']
                answer_description2= request.POST['answer_description2m']

                answer_variant3 = request.POST['answer_variant3m']
                answer_description3 = request.POST['answer_description3m']
                if 'answer_variant4m' in request.POST:
                    answer_variant4 = request.POST['answer_variant4m']
                    answer_description4 = request.POST['answer_description4m']
                    if answer_variant4 != " ":
                        ans4 = Multi_choice_answer()
                        ans4.question = question
                        ans4.multi_variant = answer_variant4
                        ans4.multi_description = answer_description4
                        if 'answer_variant5m' in request.POST:
                            answer_variant5 = request.POST['answer_variant5m']
                            answer_description5 = request.POST['answer_description5m']
                            if answer_variant5 != " ":
                                ans5 = Multi_choice_answer()
                                ans5.question = question
                                ans5.multi_variant = answer_variant5
                                ans5.multi_description = answer_description5
                                if 'answer_variant6m' in request.POST:
                                    answer_variant6 = request.POST['answer_variant6m']
                                    answer_description6 = request.POST['answer_description6m']
                                    if answer_variant6 != " ":
                                        ans6 = Multi_choice_answer()
                                        ans6.question = question
                                        ans6.multi_variant = answer_variant6
                                        ans6.multi_description = answer_description6
                check = request.POST.getlist("check")
                longeur=len(check)
                v1=0
                v2=0
                v3=0
                v4 = 0
                v5 = 0
                v6 = 0
                for i in check:
                    if i=='1':
                        v1=int(points)/longeur
                    elif i=='2':
                        v2=int(points)/longeur
                    elif i=='3':
                        v3=int(points)/longeur
                    elif i=='4':
                        v4=int(points)/longeur
                    elif i=='5':
                        v5=int(points)/longeur
                    else:
                        v6=int(points)/longeur
                multi=Multi_choice_answer()
                multi.question=question
                multi.multi_variant=answer_variant
                multi.multi_description=answer_description
                multi.points = v1

                multi.save()
                multi2= Multi_choice_answer()
                multi2.question = question
                multi2.multi_variant = answer_variant2
                multi2.multi_description = answer_description2
                multi2.points = v2

                multi2.save()
                multi3 = Multi_choice_answer()
                multi3.question = question
                multi3.multi_variant = answer_variant3
                multi3.multi_description = answer_description3
                multi3.points = v3
                multi3.save()
                if answer_variant4 != " ":
                    ans4.points=v4
                    ans4.save()
                    if answer_variant5 != " ":
                        ans5.points = v5
                        ans5.save()
                        if answer_variant6 != " ":
                            ans6.points = v6
                            ans6.save()
            return redirect('/exam/')
        else:
            return render(request, 'exam/addQuestion.html')


def deleteEXam(request,exam_id):
    exam = Exam.objects.get(id=exam_id)
    exam.delete()
    return redirect("/exam/")

def deleteQuest (request,question_id):
    question = Question.objects.get(id=question_id)
    question.delete()
    return redirect("/exam/")
l=[]
liste=[]
listevider=[]
first = 0

def passe(request,exam_id):
    exam = Exam.objects.get(pk=exam_id)
    global l
    global liste
    global first
    global listevider
    global time
    time=""
    first=0
    l=[]
    liste = []
    listevider = []

    liste += Question.objects.filter(examen=exam)  # lezem tet3ada fel page eli 9bal el as2la melowel
    shuffle(liste)  # changer le couleur apres recherche dans l si il existe
    for var in liste:
        listevider.append(var.id)
    firstid=liste[0].id
    context = {
        'examen_id': exam_id,
        'examen': exam,
        'firstid':firstid

    }
    return render(request,"exam/passeExam.html",context)
i=1
def inc_i():
    global i
    i+=1
def passeQuest(request,exam_id,Quest_id):
    #mezelet ken el question loula feha problem timer akahaw
    if request.POST :
        global liste
        listeSession=[]


        print(listeSession)
        id =request.POST['id']
        exam = Exam.objects.get(pk=exam_id)
        timer=exam.timer

        quest = Question.objects.get(pk=Quest_id)
        if int(id) in listevider:
            l.append(int(id))


        if "radios" in request.POST:
            choix =request.POST['radios']
            request.session[id] = int(choix)

        elif "radiosM" in request.POST:
            choix = request.POST.getlist('radiosM')
            request.session[id] = [int(i) for i in choix]
            print([int(i) for i in choix])

        else:
            text = request.POST['text']
            request.session[id] = text

        if int(id) in listevider:
            listevider.remove(int(id))
        length=len(liste)
        inc_i()

        if first==1:
            time=request.POST['time']
        first += 1
        if int(Quest_id) in l:
            exist=True
        else:
            exist=False
        for ques in liste:
            if str(ques.id) in request.session:
                listeSession.append([ques.id, request.session[str(ques.id)]])
        context = {
            'examen_id': exam_id,
            'examen': exam,
            'quest_id': Quest_id,
            'quest': quest,
            'liste': liste,
            'i': i,
            'id':id,
            'l':l,
            'length': length,
            'listevider': listevider,
            'first': first,
            'time':time,
            'exist': exist,
            'listeSession': listeSession,
            'timer': timer,

        }
        return render(request, "exam/QuestionExam.html",context)
    else:
        exam = Exam.objects.get(pk=exam_id)
        timer=exam.timer
        quest = Question.objects.get(pk=Quest_id)
        global liste
        global first
        global listevider
        global l
        global i
        global time
        listeSession=[]
        for ques in liste:
            if str(ques.id) in request.session:
                listeSession.append([ques.id,request.session[str(ques.id)]])
        first+=1
        if int(Quest_id) in l:
            exist=True
        else:
            exist=False
        length=len(liste)
        i = 1
        print(exist)
        context = {
            'examen_id': exam_id,
            'examen': exam,
            'quest_id': Quest_id,
            'quest': quest,
            'liste': liste,
            'i':i,
            'length':length,
            'listevider':listevider,
            'l': l,
            'first':first,
            'time': time,
            'exist':exist,
            'listeSession':listeSession,
            'timer':timer,
        }

        return render(request, "exam/QuestionExam.html", context)

def result(request,exam_id):
    pusher_client = pusher.Pusher(
        app_id='586899',
        key='fdc1d779b4030e735d1b',
        secret='4284ba41fb0893709871',
        cluster='eu',
        ssl=True
    )

    pusher_client.trigger('my-channel', 'my-event', {'message': 'hello world'})
    score=0
    listeScore=[]
    exam = Exam.objects.get(pk=exam_id)
    listeScore += Question.objects.filter(examen=exam)
    for question in listeScore:


        if str(question.id) in request.session:
            if type(request.session[str(question.id)]) is list:
                for choixia in request.session[str(question.id)]:
                    print('liste')
                    print(choixia)
                    rep = Reponse()
                    rep.user = request.user
                    rep.exam = exam
                    Mans = Multi_choice_answer.objects.get(pk=choixia)
                    one = one_answer()
                    one.id = 0
                    rep.One_answer = one
                    rep.multi_choice_answer = Mans
                    rep.score_question = Mans.points
                    rep.save()
            elif type(request.session[str(question.id)]) is int:
                rep = Reponse()
                rep.user = request.user
                rep.exam = exam
                ans = one_answer.objects.get(pk=int(request.session[str(question.id)]))
                mult = Multi_choice_answer()
                mult.id = 0
                rep.multi_choice_answer = mult
                rep.One_answer = ans
                rep.score_question = ans.points
                rep.save()
            elif type(request.session[str(question.id)]) is str:
                rep = Reponse()
                rep.user = request.user
                rep.exam = exam
                one = one_answer()
                one.id = 0
                rep.One_answer = one
                mult = Multi_choice_answer()
                mult.id = 0
                rep.multi_choice_answer = mult
                rep.free_text = request.session[str(question.id)]
                rep.save()
            del request.session[str(question.id)]

    listeRep=Reponse.objects.filter(exam=exam , user=request.user)
    for res in listeRep:
        score+=res.score_question
    if score >= exam.score:
        certif = certificat()
        certif.exam = exam
        certif.student = request.user
        certif.score = score
        certif.save()



    context={
        'username':request.user,
        'examen_id': exam_id,
        'exam': exam,
        'score':score,
    }

    return render(request,"exam/result.html",context)

def invitating(request,exam_id):
    if request.POST :
        studentclass = studentClass.objects.filter(professor=request.user)
        exam =Exam.objects.get(pk=exam_id)
        print('send')

        invites=(request.POST.getlist("user"))
        for invite in invites:
            inv =invitation()
            inv.prof=request.user
            inv.exams=exam
            inv.student=User.objects.get(pk=invite)
            inv.save()

        return redirect('/exam')

    studentclass = studentClass.objects.filter(professor=request.user)
    context={
        "profiles":studentclass,
    }
    return render(request,'exam/invitation.html',context)

#consulter et filtrer les etudiants
def consultUsers(request):
    studentclass=studentClass.objects.filter(professor=request.user)
    print(studentclass)

    context = {
        "profiles": studentclass,
        'user':request.user,
    }
    return render(request, 'exam/consultUsers.html',context)

#ajouter un user au classe de prof pour qu il puisse l affecter a son devoir
def addUser(request):
    if request.POST:
        print("aaaaaaaaaaaa")
        affected=request.POST.getlist("affected")
        for i in affected:
            studentclass = studentClass()
            studentclass.professor=request.user
            studentclass.student=User.objects.get(pk=int(i))
            studentclass.save()
        return redirect('/exam/')


#probleme
    for students in studentClass.objects.filter():
        if students.student == None :
            try:
                user =User.objects.get(email=students.email_invited)
                students.student=user
                students.save()
            # do what you are trying
                print('yessssssssssssss')
            except ObjectDoesNotExist:
                # print something or the whole stack

                print("ehiiiiiiiiiiiiii")
                #verifier si l objet existe sinon ne rien faire
        else:
            print(students.student)
    profiles = Profile.objects.all()
    context={
        'profiles':profiles,
    }

    return render(request, 'exam/addUser.html',context)

#envoir un mail pour les etudiants et au meme temps l affecter au classe de prof
def sendMail(request):
    if request.POST:
        send = request.POST['mail']
        s = send.split(',')
        for sending in s :
            studentclass = studentClass()
            studentclass.professor = request.user
            studentclass.email_invited = sending
            studentclass.save()
        print('send')

        send_mail('object',
                  'contenu',
                  'quiz@ghazelatc.com',
                  s,
                  fail_silently=False
                  )
        return redirect('/exam/addUser')
    return render(request,'exam/sendMail.html')

# cette fonction est principale puisqu elle permet de classer les questions dans un ordre aleatoire et de valider le timer
def passeStudent(request,exam_id):
    exam = Exam.objects.get(pk=exam_id)
    global l
    global liste
    global first
    global listevider
    global time
    time=""
    first=0
    l=[]
    liste = []
    listevider = []

    liste += Question.objects.filter(examen=exam)  # lezem tet3ada fel page eli 9bal el as2la melowel
    shuffle(liste)  # changer le couleur apres recherche dans l si il existe
    for var in liste:
        listevider.append(var.id)
    firstid=liste[0].id
    context = {
        'examen_id': exam_id,
        'examen': exam,
        'firstid':firstid

    }
    return render(request,"exam/PasseExamStudent.html",context)

#le passage de l examen
def passeQuestStudent(request,exam_id,Quest_id):
    #mezelet ken el question loula feha problem timer akahaw
    if request.POST :
        global liste
        listeSession=[]


        print(listeSession)
        id =request.POST['id']
        exam = Exam.objects.get(pk=exam_id)
        if exam.type =='training':
            training=True
        else:
            training=False
        timer=exam.timer

        quest = Question.objects.get(pk=Quest_id)
        if int(id) in listevider:
            l.append(int(id))


        if "radios" in request.POST:
            choix =request.POST['radios']
            request.session[id] = int(choix)

        elif "radiosM" in request.POST:
            choix = request.POST.getlist('radiosM')
            request.session[id] = [int(i) for i in choix]
            print([int(i) for i in choix])

        else:
            text = request.POST['text']
            request.session[id] = text

        if int(id) in listevider:
            listevider.remove(int(id))
        length=len(liste)
        inc_i()

        if first==1:
            time=request.POST['time']
        first += 1
        if int(Quest_id) in l:
            exist=True
        else:
            exist=False
        for ques in liste:
            if str(ques.id) in request.session:
                listeSession.append([ques.id, request.session[str(ques.id)]])

        context = {
            'examen_id': exam_id,
            'examen': exam,
            'quest_id': Quest_id,
            'quest': quest,
            'liste': liste,
            'i': i,
            'id':id,
            'l':l,
            'length': length,
            'listevider': listevider,
            'first': first,
            'time':time,
            'exist': exist,
            'listeSession': listeSession,
            'timer': timer,
            'training':training,

        }
        return render(request, "exam/QuestExamStudent.html",context)
    else:
        exam = Exam.objects.get(pk=exam_id)
        if exam.type =='training':
            training=True
        else:
            training=False
        timer=exam.timer

        quest = Question.objects.get(pk=Quest_id)
        global liste
        global first
        global listevider
        global l
        global i
        global time
        listeSession=[]
        for ques in liste:
            if str(ques.id) in request.session:
                listeSession.append([ques.id,request.session[str(ques.id)]])
        first+=1
        if int(Quest_id) in l:
            exist=True
        else:
            exist=False
        length=len(liste)
        i = 1
        print(exist)
        context = {
            'examen_id': exam_id,
            'examen': exam,
            'quest_id': Quest_id,
            'quest': quest,
            'liste': liste,
            'i':i,
            'length':length,
            'listevider':listevider,
            'l': l,
            'first':first,
            'time': time,
            'exist':exist,
            'listeSession':listeSession,
            'timer': timer,
            'training': training,

        }

        return render(request, "exam/QuestExamStudent.html", context)

#resultat calculer et afficher aussi si le score obtenu debasse le score de l examen une certifcat est geré si l examen est certifier
#aussi l instalation des reponses dans une base pour que l etudiant puisse etre valider par le professeur
def resultStudent(request,exam_id):
    # pusher_client = pusher.Pusher(
    #     app_id='586899',
    #     key='fdc1d779b4030e735d1b',
    #     secret='4284ba41fb0893709871',
    #     cluster='eu',
    #     ssl=True
    # )
    #
    # pusher_client.trigger('my-channel', 'my-event', {'message': request.user.username})
    score=0
    listeScore=[]
    exam = Exam.objects.get(pk=exam_id)
    invite = invitation.objects.get(student=request.user,exams=exam)

    newAttempt=invite.attempt+1
    invite.attempt=newAttempt

    listeScore += Question.objects.filter(examen=exam)
    if exam.type=='one':
        invite.non_passed=False
    invite.save()

    for question in listeScore:


        if str(question.id) in request.session:
            if type(request.session[str(question.id)]) is list:
                for choixia in request.session[str(question.id)]:
                    print('liste')
                    print(choixia)
                    rep = Reponse()
                    rep.user = request.user
                    rep.exam = exam
                    Mans = Multi_choice_answer.objects.get(pk=choixia)
                    one = one_answer()
                    one.id = 0
                    rep.One_answer = one
                    rep.multi_choice_answer = Mans
                    rep.score_question = Mans.points
                    rep.nbr_attempt=newAttempt
                    rep.save()
            elif type(request.session[str(question.id)]) is int:
                rep = Reponse()
                rep.user = request.user
                rep.exam = exam
                ans = one_answer.objects.get(pk=int(request.session[str(question.id)]))
                mult = Multi_choice_answer()
                mult.id = 0
                rep.multi_choice_answer = mult
                rep.One_answer = ans
                rep.score_question = ans.points
                rep.nbr_attempt = newAttempt
                rep.save()
            elif type(request.session[str(question.id)]) is str:
                rep = Reponse()
                rep.user = request.user
                rep.exam = exam
                one = one_answer()
                one.id = 0
                rep.One_answer = one
                mult = Multi_choice_answer()
                mult.id = 0
                rep.multi_choice_answer = mult
                rep.free_text = request.session[str(question.id)]
                #comparaison simple entre text et rep.freetext
                rep.nbr_attempt = newAttempt
                rep.save()
            del request.session[str(question.id)]

    listeRep=Reponse.objects.filter(exam=exam , user=request.user,nbr_attempt=newAttempt)
    for res in listeRep:
        score+=res.score_question
    if score >= exam.score:
        certif = certificat()
        certif.exam = exam
        certif.student = request.user
        certif.score = score

        certif.save()
    context={
        'username':request.user,
        'examen_id': exam_id,
        'exam': exam,
        'score':score,
    }

    return render(request,"exam/resultatStudent.html",context)

#suite a un click sur le bouton le user  transforme  d un etudiant a un professeur
def becameProf(request):
    if request.user.is_authenticated():
        profile=Profile.objects.get(user=request.user)
        profile.is_prof=True
        profile.save()
        all_exam = Exam.objects.filter(user=request.user)
        if id in request.session:
            del request.session['id']
        my_template = 'exam/baseProf.html'

        context = {
            'my_template': my_template,
            'all_exam': all_exam,
        }
        return render(request, 'exam/index.html', context)


    else:
        return render(request, 'authentification/home.html')

#suite a l inviation du prof l etudant trouve ses examens (le style training reste toujours sauf a la suppression de l examen)
def MyExams(request):
    invite = invitation.objects.filter(student=request.user)
    print(invite)
    my_template="exam/baseProf.html"
    context = {
        'invite': invite,
        'prof':True,
        'my_template':my_template,
    }
    return render(request, 'exam/studentIndex.html', context)

#afficher tous les examens corriger et passer
def correction(request):
    invite = invitation.objects.filter(student=request.user)
    profile = Profile.objects.get(user=request.user)
    if profile.is_prof == True:
        my_template = "exam/baseProf.html"
    else:
        my_template="exam/base.html"
    context = {
        'invite': invite,
        'my_template': my_template,
    }
    return render(request,'exam/correction.html',context)

#afficher la correstions de l examen en question
def correctionExam(request,exam_id):
    exam = Exam.objects.get(pk=exam_id)
    profile = Profile.objects.get(user=request.user)
    if profile.is_prof == True:
        my_template = "exam/baseProf.html"
    else:
        my_template = "exam/base.html"
    context = {
        'exam': exam,
        'my_template': my_template,
    }
    return render(request,'exam/correctionExam.html',context)

#generer une page qui affiche tout les certificats obtenu par l etudiant
def certified(request):
    certif = certificat.objects.filter(student=request.user)
    profile = Profile.objects.get(user=request.user)
    if profile.is_prof == True:
        my_template = "exam/baseProf.html"
    else:
        my_template = "exam/base.html"
    context = {
        'certif': certif,
        'my_template': my_template,
    }
    return render(request,'exam/certificat.html',context)

#permer d imprimer la certificat en question
def imprimeCertif(request,exam_id):
    exam = Exam.objects.get(pk=exam_id)
    profile = Profile.objects.get(user=request.user)
    if profile.is_prof == True:
        my_template = "exam/baseProf.html"
    else:
        my_template = "exam/base.html"
    context = {
        'user':request.user,
        'exam': exam,
        'my_template': my_template,
    }
    return render(request,'exam/imprimeCertif.html',context)

def profile(request):
    profile = Profile.objects.get(user=request.user)

    if profile.is_prof == True:
        my_template = "exam/baseProf.html"
    else:
        my_template = "exam/base.html"
    context = {
        'my_template': my_template,
        'profile':profile,
    }
    return render(request, 'exam/profile.html', context)


def updateProfile(request):

    profile = Profile.objects.get(user=request.user)
    user=User.objects.get(pk=request.user.id)
    if request.POST:
        username = request.POST['username']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        location = request.POST['location']
        if 'image' in request.FILES:
            image = request.FILES['image']
            profile.image = image
        user.username = username
        user.first_name = first_name
        user.last_name = last_name
        profile.location=location
        user.save()
        profile.save()
        return redirect('/exam/profile')

    if profile.is_prof == True:
        my_template = "exam/baseProf.html"
    else:
        my_template = "exam/base.html"
    context = {
        'my_template': my_template,
        'profile':profile,
    }
    return render(request, 'exam/updateProfile.html', context)
