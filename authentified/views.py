from django.shortcuts import render, redirect# Create your views here.
from django.contrib.auth import authenticate, get_user_model,login,logout
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm
from django.contrib import auth

from django.views.generic import View
from .forms import userloginform ,UserRegisterForm


def index(request):
    template = loader.get_template('authentification/index.html')
    context = {
        'all_exam':'aaa',
    }
    return  HttpResponse(template.render(context,request))

def login_view(request):
    print(request.user.is_authenticated())
    title="login"
    form =userloginform(request.POST or None)
    if form.is_valid():
        username=form.cleaned_data.get("username")
        password=form.cleaned_data.get("password")
        user =authenticate(username=username,password=password)
        login(request,user)
        print(request.user.is_authenticated())
        return render(request, 'exam/studentIndex.html')
    return render(request,"firstPage/index.html",{"form":form,"title":title})


def register_view(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/exam')
    else:
        form = SignUpForm()
    return render(request, 'authentification/register.html', {'form': form})

def logout(request):
    auth.logout(request)
    return render(request,'firstPage/index.html')
