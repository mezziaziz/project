from django.contrib.auth import authenticate, get_user_model,login,logout
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Profile

User = get_user_model()

class userloginform(forms.Form):
    username=forms.CharField()
    password=forms.CharField(widget=forms.PasswordInput)

    def clean(self,*args,**kwargs):
        username =self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        user = authenticate(username=username,password=password)
       # user_qs=User.objects.filter(username=username)
       # if user_qs.count()==1:
        #    user = user_qs.first()
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError("this user does not exist")
            if not user.check_password(password):
                raise forms.ValidationError("password incorrect")
            if not user.is_active:
                raise forms.ValidationError("this user not active")
        return super(userloginform,self).clean(*args,**kwargs)

class UserRegisterForm(forms.ModelForm):
    email =forms.EmailField(label='Email adresse :')
    email2 =forms.EmailField(label='confirm Email :')
    password=forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model =User
        fields=[
            'username',
            'email',
            'email2',
            'password'
        ]

    def clean_email(self,*args,**kwargs):
        email=self.cleaned_data.get('email')
        email2=self.cleaned_data.get('email2')
        if email!=email2:
            raise forms.ValidationError("Email must match")
        email_qs=User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError('this email exists ')
        return super(UserRegisterForm,self).clean(*args,*kwargs)

    def clean_email(self):
        email=self.cleaned_data.get('email')
        email2=self.cleaned_data.get('email2')
        if email!=email2:
            raise forms.ValidationError("Email must match")
        email_qs=User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError('this email exists ')
        return email

class SignUpForm(UserCreationForm):

    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    def clean_email(self):
        email=self.cleaned_data.get('email')
        email_qs=User.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError('this email exists ')
        return email
    class Meta:
        model = User
        fields = ('username',  'email', 'password1', 'password2')

