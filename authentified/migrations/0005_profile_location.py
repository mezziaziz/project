# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2018-08-30 10:37
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentified', '0004_profile_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='location',
            field=models.CharField(default='', max_length=1000),
        ),
    ]
