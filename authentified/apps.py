from django.apps import AppConfig


class AuthentifiedConfig(AppConfig):
    name = 'authentified'
