from django.conf.urls import url
from . import views
from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView
from django.contrib import admin

urlpatterns = [
    url(r'^$', views.login_view , name='login'),
    url(r'^register/$', views.register_view, name='register'),
    url(r'^log/$', auth_views.login, {'template_name': 'authentification/index.html'}, name='log'),
    url(r'^logout/$', views.logout, name='logout')
]